5-row squeekboard terminal layout that i modified from this: https://source.puri.sm/btantau/squeekboard/blob/btantau-master-patch-76686/data/keyboards/terminal.yaml

You may want to change key heights depending on what scale you have in /etc/phosh/phoc.ini. I made it for 1.8 scale.

<img src="layout.png" />